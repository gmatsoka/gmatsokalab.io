function getPrices() {
    return {
        prodTypes: [75000, 170000, 240000],
        prodOptions: {
            option1: 0,
            option2: 30000,
            option3: 33000
        },
        prodProperties: {
            prop1: 50000,
            prop2: 11500,
        }
    };
}

function updatePrice() {
    let s = document.getElementsByName("prodType");
    let select = s[0];
    let price = 0;
    let prices = getPrices();
    let priceIndex = parseInt(select.value) - 1;
    if (priceIndex >= 0) {
        price = prices.prodTypes[priceIndex];
    }

    let imageDiv1 = document.getElementById("m1");
    imageDiv1.style.display = (select.value === "1"
                               ? "block"
                               : "none"
    );

    let imageDiv2 = document.getElementById("hover");
    imageDiv2.style.display = (select.value === "2"
                               ? "block"
                               : "none"
    );

    let imageDiv3 = document.getElementById("m3");
    imageDiv3.style.display = (select.value === "3"
                               ? "block"
                               : "none"
    );

    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = (
        select.value === "2"
        ? "block"
        : "none"
    );

    if (select.value === "2") {
        let zoom = document.getElementsByName("prodOptions");
        zoom.forEach(function (radio) {
            if (radio.checked) {
                let optionPrice = prices.prodOptions[radio.value];
                price += optionPrice;
            }
        });
    }

    let checkDiv = document.getElementById("checkboxes");
    checkDiv.style.display = (
        select.value === "3"
        ? "block"
        : "none"
    );

    if (select.value === "3") {
        let functions = document.querySelectorAll("#checkboxes input");
        functions.forEach(function (checkbox) {
            if (checkbox.checked) {
                let propPrice = prices.prodProperties[checkbox.name];
                price += propPrice;
            }
        });
    }
    let number = document.getElementById("number_");

    let prodPrice = document.getElementById("prodPrice");
    prodPrice.innerHTML = number.value * price;
}

window.addEventListener("DOMContentLoaded", function () {

    updatePrice();
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";

    let s = document.getElementsByName("prodType");
    let select = s[0];
    select.addEventListener("change", function () {
        updatePrice();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function () {
            updatePrice();
        });
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function () {
            updatePrice();
        });
    });
    let number = document.getElementById("number_");
    number.addEventListener("input", function () {
        updatePrice();
    });

});